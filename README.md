# pz80mon
## Overview
pz80mon is a project aimed at porting the functionality of the original Apple I Wozmon, written by Steve Wozniak, to Z80 based computers. It strives to maintain all the original features and operations of Wozmon, adapting them for the Z80 architecture.

## Installation and Setup
### Prerequisites
- z88dk toolchain
### Compilation
Compile the project using:

```bash
make
```

### Flashing to ROM
To write the compiled code to ROM, use:

```bash
make flash
```
This uses `minipro`. Note that users may need to adjust the RESET and ECHO routines in the code to match their machine's I/O hardware specifics.

## Usage
pz80mon supports the following modes:

- Examine
- Block Examine
- Store
- Run

## Contributing
Contributions are welcome! If you'd like to contribute, please submit Merge Requests to our GitLab repository. Although there are no formal contributing guidelines at the moment, we appreciate your interest in improving pz80mon.

## Contact
For support, inquiries, or to report issues, please submit Issues on our GitLab repository.

## Acknowledgements
Special thanks to Steve Wozniak for the original Wozmon code. Kudos to cjs from the "8 Bit World" Discord server for their invaluable assistance in troubleshooting.
