;------------------------------------------------------------------------------
; PZ80 monitor program based on WOZMon for Z80 CPU
; Written by Peter Ezetta - 2024
; Original 6502 WOZMon written by Steve Wozniak - 1976
;
; 6502 only has 3x 8-bit registers, A, X, and Y.
; X and Y are both 8 bit index registers.
;
; We need HL to point to memory addresses.
; We will use BC and DE as X and Y respectively
;------------------------------------------------------------------------------

MODULE monitor
ORG $0000

;------------------------------------------------------------------------------
; Memory Addresses
;------------------------------------------------------------------------------

	DEFC	SIO_DATA_A = $00	; address of SIO A/D
	DEFC	SIO_CTRL_A = $02	; address of SIO A/C
	DEFC	SIO_CTRL_B = $03	; address of SIO B/C

	DEFC	XAML = $8000		; Last "opened" location Low
	DEFC	XAMH = $8001		; Last "opened" location High
	DEFC	STL = $8002		; Store address Low
	DEFC	STH = $8003		; Store address High
	DEFC	HEXL = $8004		; Hex value parsing Low
	DEFC	HEXH = $8005		; Hex value parsing High
	DEFC	ESAV = $8006		; Used to see if hex value is given
	DEFC	MODE = $8007		; $00=XAM, $7F=STOR, $AE=BLOCK XAM
	DEFC	IN = $8100		; Input Buffer:  $8100-$81FF

;------------------------------------------------------------------------------
; Constants
;------------------------------------------------------------------------------

	DEFC	BS=$08			; backspace
	DEFC	CR=$0D			; carriage return
	DEFC	ESC=$1B			; escape

SECTION CODE

RESET:
	LD	SP, $FFFF		; set the stack pointer to top of RAM
	LD	A, %00011000		; store channel reset command in A
	OUT	(SIO_CTRL_A), A		; send it to SIO channel A
	LD	HL, SIO_INIT_STR        ; store location of init str in HL
	LD	C, (HL)                 ; address of SIO port in c
	INC	HL                      ; point at the length
	LD	B, (HL)                 ; length of byte string
	INC	HL                      ; point at the bytestring
	OTIR                            ; send bytes

; SIO and CPU are initialized at this point

	XOR	A			; initialize A with $0
	LD	DE, $007F		; initialize E with $7F

;------------------------------------------------------------------------------
; The GETLINE process
;------------------------------------------------------------------------------

NOTCR:
	CP	BS			; does A contain a backspace?
	JR	Z, BACKSPACE		; yes
	CP	ESC			; does A contain an escape?
	JR	Z, ESCAPE		; yes
	INC	E			; increment the buffer
	JP	P, NEXTCHAR		; if we didn't overflow, jump to NEXTCHAR

ESCAPE:
	LD	A, '\\'			; load the prompt to A
	CALL	ECHO			; send it!

GETLINE:
	LD	A, CR			; load a CR to A
	CALL	ECHO			; send it!

	LD	E, $1			; initialize E with 1
BACKSPACE:
	DEC	E			; dec E
	JP	M, GETLINE		; if we E is negative, go back to GETLINE

NEXTCHAR:
	XOR	A			; select RR0 (%00000000)
	OUT	(SIO_CTRL_A), A		; Send it to SIO
	IN	A, (SIO_CTRL_A)		; read RR0
	BIT	0, A			; test for char available
	JR	Z, NEXTCHAR		; no char available, loop

	IN	A, (SIO_DATA_A)		; get the char from the SIO
	LD	HL, IN			; store beginning of buffer in HL
	ADD	HL, DE			; add the offset
	LD	(HL), A			; store the char in the buffer
	CALL	ECHO			; echo to term
	CP	CR			; is it a CR?
	JR	NZ, NOTCR		; no.

; Line received, lets parse it!

	LD	DE, $00FF 		; Reset the buffer pointer
	XOR	A			; A = 0
	LD	BC, 0			; C = 0

SETBLOCK:
	SLA	A			; '.' = 00101110 -> 01011100

SETSTOR:
	SLA	A			; ':' = 00111010 -> 01110100 or '.' shifts again
					; 01011100 -> 10111000
SETMODE:
	LD	(MODE), A		; $00 = XAM, $74 = STOR, $B8 = BLOK XAM

BLKSKIP:
	INC	E			; Skip. Next item in buffer

NEXTITEM:
	LD	HL, IN			; Load the buffer address to HL
	ADD	HL, DE			; Offset into the buffer
	LD	A, (HL)			; Load the next char in the buffer to A
	CP	CR			; Is it a CR?
	JR	Z, GETLINE		; Yes. We're done.
	CP	'.'			; Is it a '.'?
	JR	C, BLKSKIP		; Less than '.' -- skip
	JR	Z, SETBLOCK		; Yes. Set mode to BLKXAM
	CP	':'			; Is it a ':'?
	JR	Z, SETSTOR		; Yes. Set mode to STOR
	CP	'R'			; Is it an 'R'?
	JR	Z, RUN			; Yes.
	XOR	A			; Zero out A
	LD	(HEXL), A		; Zero out HEXL
	LD	(HEXH), A		; Zero out HEXH
	LD	A, E			; Store index in A
	LD	(ESAV), A		; Save index to RAM

; Time to parse a new hex value

NEXTHEX:
	LD	HL, IN			; Address of IN to HL
	ADD	HL, DE			; Point into IN + Offset
	LD	A, (HL)			; Load value at IN + Offset to A
	XOR	$30			; Map value to Digit
	CP	$9 + 1			; Less than 10?
	JR	C, DIG			; Yes, it's a digit
	ADD	$89			; Remap $A-$F to $FA-$FF
	CP	$FA			; Less than $FA?
	JR	C, NOTHEX		; Yes. Not hex.

DIG:
	SLA	A			; Shift digit into MSB of A
	SLA	A
	SLA	A
	SLA	A

	LD	C, 4			; Shift count
HEXSHIFT:
	SLA	A			; Hex digit left, MSB to carry
	LD	HL, HEXL		; Load address of HEXL to HL
	RL	(HL)			; Rotate into LSD
	LD	HL, HEXH		; Load address of HEXH to HL
	RL	(HL)			; Rotate into MSD
	DEC	C			; Done 4 shifts?
	JR	NZ, HEXSHIFT		; No, loop.
	INC	E			; Advance the index
	JR	NEXTHEX			; Check next char for hex.

NOTHEX:
	LD	A, (ESAV)		; Load the value of ESAV to A
	CP	E			; Compare it with E
	JP	Z, ESCAPE		; If they match, there was no input, restart

	LD	HL, MODE		; Load the address of MODE to HL
	BIT	6, (HL)			; Check for Bit 6
	JR	Z, NOTSTOR		; No bit 6, not in STOR mode

; STOR mode, save LSD of new hex byte.

	LD	A, (HEXL)		; Load HEXL to A
	LD	HL, (STL)		; point at STL's value
	ADD	HL, BC			; add the offset
	LD	(HL), A			; Load A to STL
	LD	HL, STL			; point at STL
	INC	(HL)			; Increment STL
	JR	NZ, NEXTITEM		; No Carry!
	LD	HL, STH			; Load STH to HL
	INC	(HL)			; Increment STH for carry

TONEXTITEM:
	JR	NEXTITEM		; Get next command item.

;-------------------------------------------------------------------------------
; RUN user's program from last opened location
;-------------------------------------------------------------------------------

RUN:
	LD	HL, (XAML)		; Load the address stored at XAML to HL
	JP	(HL)			; Jump there.

;-------------------------------------------------------------------------------
; We're not in STOR mode
;-------------------------------------------------------------------------------

NOTSTOR:
	BIT	7, (HL)
	JR	NZ, XAMNEXT		; B7 = 0 for XAM, 1 for BLOCK XAM

; We're in XAM mode now
	
	LD	BC, 2			; Storing 2 bytes
SETADR:
	LD	HL, HEXL - 1		; Store address of HEXL - 1 in HL
	ADD	HL, BC			; Offset address by count
	LD	A, (HL)
	LD	HL, STL - 1		; Copy hex data to store index
	ADD	HL, BC
	LD	(HL), A
	LD	HL, XAML - 1		; And to XAM index
	ADD	HL, BC
	LD	(HL), A
	DEC	C			; Next byte
	JR	NZ, SETADR		; Loop unless C == 0

; Print address and data from this address, fall through next JR NZ

NXTPRNT:
	JR	NZ, PRDATA
	LD	A, CR			; Load a CR to A
	CALL	ECHO			; Print it
	LD	A, (XAMH)		; Load the value of XAMH to A
	CALL	PRBYTE			; Call PRBYTE to print it
	LD	A, (XAML)		; Load the value of XAML to A
	CALL	PRBYTE			; Call PRBYTE to print it
	LD	A, ':'			; Load a : to A
	CALL	ECHO			; Print it

PRDATA:
	LD	A, ' '			; Load a ' ' to A
	CALL	ECHO			; Print it
	LD	HL, (XAML)		; Load the address of XAML to HL
	ADD	HL, BC			; Add the offset to that address
	LD	A, (HL)			; Load the value at that offset to A
	CALL	PRBYTE			; Call PRBYTE to print it

XAMNEXT:
	LD	HL, MODE		; Load address of MODE to HL
	LD	(HL), 0			; Set mode to 0 (XAM)

	PUSH	DE			; Save DE
	OR	A			; Clear Carry Flag
	LD	HL, (XAML)		; Load value of XAML to HL
	LD	DE, (HEXL)		; Load value of HEXL to DE
	SBC	HL, DE			; Compare XAML and HEXL
	POP	DE			; Restore DE
	JR	NC, TONEXTITEM		; Jump to next item if XAML >= HEXL

	LD	HL, XAML		; Load address of XAML to HL
	INC	(HL)			; Increment value of XAML
	JR	NZ, MOD8CHK		; Check for mod 8
	LD	HL, XAMH		; Load address of XAMH to HL
	INC	(HL)			; Increment XAMH

MOD8CHK:
	LD	A, (XAML)
	AND	0b00000111
	JR	NXTPRNT

PRBYTE:
	PUSH	AF			; Save the AF registers
	SRL	A			; Rotate LSD to MSD
	SRL	A
	SRL	A
	SRL	A
	CALL	PRHEX			; Call PRHEX routine
	POP	AF			; Restore AF register

PRHEX:
	AND	A, 0b00001111		; Mask LSD
	OR	A, $30			; Map back to ASCII
	CP	$3A			; Digit?
	JR	C, ECHO			; Yes, print.
	ADD	$7			; No, map to char

; fall through to echo process

;-------------------------------------------------------------------------------
; Echo process
;-------------------------------------------------------------------------------

ECHO:
	OUT	(SIO_DATA_A), A		; send the char in A
	PUSH	AF			; save AF
ECHOWAIT:
	XOR	A	 		; select RR0
	OUT     (SIO_CTRL_A), A         ; send the pointer
	IN      A, (SIO_CTRL_A)         ; read byte from RR0
	BIT     2, A                    ; test for the tx buffer being empty
	JR      Z, ECHOWAIT		; loop until buffer becomes empty
	POP	AF			; restore AF
	RET

SECTION DATA

SIO_INIT_STR:
        defb    SIO_CTRL_A
        defb    8                       ; send 8 commands to A
        defb    %00000100               ; select WR4
        defb    %01000100               ; 16x clock, 1 stop bit, no parity
        defb    %00000001		; select WR1
        defb    %00000000               ; disable interrupts
        defb    %00000011               ; select WR3
        defb    %11000001               ; 8 bits, auto enables off, enable rx
        defb    %00000101               ; select WR5
        defb    %11101000               ; DTR active, 8 bits, enable tx
